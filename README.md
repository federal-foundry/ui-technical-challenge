# Federal Foundry UI Technical Challenge

## Overview
For this technical challenge, your task is to write a React application that will list and filter provided professional league data. Mocked data is available under `./data/`. 

## Requirements
- Use `create-react-app` to generate an application
- Mock a `fetch` call using the provided data as a response (no backend server, just import the data)
- Display each team within a league in a card format showing pieces from the provided data (e.g. team name, location, etc)
- The page must have a set of filters for each league within the data (check out the mocks for more information)
- Filters will show/hide a league based on a given state (how you default the state is up to you)

## Notes
- Not all data must be displayed
- This is meant to be a quick exercise and should take 1-2 hours to complete
- Unit tests are optional (note that we **do** test code here and you will be expected to write your own tests)
- We're looking at how you write code and structure applications, not 100% correctness (it's a short exercise, don't stress too much)


## Example Team Display
You do **not** need to match this style, this is merely an example of what a team could look like.

![](team-example.png)

